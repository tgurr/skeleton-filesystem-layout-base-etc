VERSION = 0.87

all: skeleton-filesystem-layout-base-etc-$(VERSION).tar.xz

clean:
	@echo "Cleaning ..."
	@rm -rf *.tar.xz *.tar || true

skeleton-filesystem-layout-base-etc-$(VERSION).tar.xz:
	@echo "Generating $@ ..."
	@git archive --prefix=skeleton-filesystem-layout-base-etc-$(VERSION)/ HEAD | xz -z > $@

upload: skeleton-filesystem-layout-base-etc-$(VERSION).tar.xz
	@echo "Uploading $< ..."
	scp $< dev.exherbo.org:/srv/www/dev.exherbo.org/distfiles/skeleton-filesystem-layout/

.default: all

.phony: clean upload
